var arbol = new Arbol();
var tp = [] , z , camino = '' , suma = 0;


//llenamos el arbolito
arbol.NodoPadre.Ni = arbol.CrearNodo(arbol.NodoPadre, "Izquierdo", arbol.NodoPadre.nivel, 36);

arbol.NodoPadre.Nd = arbol.CrearNodo(arbol.NodoPadre, "Derecho", arbol.NodoPadre.nivel, 20);

arbol.NodoPadre.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni, "Izquierdo", arbol.NodoPadre.Ni.nivel, 16);

arbol.NodoPadre.Ni.Nd = arbol.CrearNodo(arbol.NodoPadre.Ni, "Derecho", arbol.NodoPadre.Ni.nivel, 12);

arbol.NodoPadre.Ni.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni.Nd, "Izquierdo", arbol.NodoPadre.Ni.Nd.nivel, 8);

arbol.NodoPadre.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd, "Izquierdo", arbol.NodoPadre.Nd.nivel, 5);

arbol.NodoPadre.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd, "Derecho", arbol.NodoPadre.Nd.nivel, 4);

arbol.NodoPadre.Nd.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, "Izquierdo", arbol.NodoPadre.Nd.Nd.nivel, 2);

arbol.NodoPadre.Nd.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, "Derecho", arbol.NodoPadre.Nd.Nd.nivel, 2);


//imprime el arbol
console.log(arbol);

//se realiza la busqueda por nivel
var nivel = prompt("INGRESE UN NIVEL:");
BusquedaNivel(arbol.NodoPadre, nivel);
console.log("ELEMENTOS EN EL NIVEL " + nivel + " : " + tp);

//se realiza la busqueda por elemento
var nel = prompt("INGRESE VALOR A BUSCAR:");
console.log("EL ELEMENTO " + nel + " SE ENCUENTRA EN EL NODO: ");

var nodox = BusquedaElemento(arbol.NodoPadre, nel);
console.log(nodox);

//Camino de nodo
var cam = CaminoNodo(nodox, nel);
console.log(cam);

//Se suman los elementos
var sum = CostoNodo(nodox, nel);
console.log(sum);

//se realizan las funciones 

function BusquedaNivel(Nodo, nivel){
    
    if(Nodo.nivel == nivel)
        tp.push(Nodo.valor);

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaNivel(Nodo.Ni, nivel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaNivel(Nodo.Nd, nivel);

}

function BusquedaElemento(Nodo, nel){

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaElemento(Nodo.Ni, nel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaElemento(Nodo.Nd, nel);

    if(Nodo.valor == nel){
        z = Nodo;
    }

    return z;
}

function CaminoNodo(Nodo, nel){

    if(Nodo.padre != null){
        camino = camino + ' ' + Nodo.padre.valor;
        CaminoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + camino;

}

function CostoNodo(Nodo, nel){

    if(Nodo.padre != null){
        suma = suma + Nodo.padre.valor;
        CostoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + suma;

}
