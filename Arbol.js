

//se declara el metodo//
class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
    }

    CrearPadre(padre = null, tipo = "central", nivel = null, valor = 15){
        var nodo = new Nodo(padre, tipo, nivel, valor);
        return nodo;
    }
    
    CrearNodo(padre, tipo, nivel, valor){
        var nodo = new Nodo(padre, tipo, nivel, valor);
        return nodo;
    }
}
